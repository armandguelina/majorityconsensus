# MajorityConsensus

This application computes election results using the Majority Consensus method.
It takes either a `.csv` file  followed by option 0 or a csvString with ; as delimeter followed by option 1 containing the sum of all the votes for each candidate as parameter, such as:

For a .csv file as parameter:

```csv
Candidates,Reject,Poor,Acceptable,Good,VeryGood,Excellent
Y,20,10,10,20,10,30
Z,20,40,10,10,15,5
X,20,20,30,10,15,5
A,15,15,15,15,15,25
K,30,20,10,10,15,5
```

you get a .tsv file output:

```tsv
Candidates	Grades	Rank
Z	75	1
K	72	2
X	-5	3
Y	-20	4
A	-25	5
```

The input can come either from a file, or stdin (using the `--stdin`) flag, and
the output can go either to a file, by default `Results.tsv` or stdout (using
the `--stdout` flag).

Supposing it is compiled using `javac MajorityConsensus.java`, some possible
variations are:

```
> cat input.csv | java MajorityConsensus --stdin
> cat input.csv | java MajorityConsensus --stdin --stdout
> java MajorityConsensus input.csv
> java MajorityConsensus input.csv output.tsv
```
